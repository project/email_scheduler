<?php   
function email_scheduler_form($form, &$form_state) {
  $select_type = [
    'Roles' => t("Roles"),
    'Users' => t("Individual Users"),
  ];
  
  $form['email_scheduler_settings'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Please select which type of gourp you want to enable to send emails'),
    '#options' =>  $select_type,
    '#default_value' => variable_get('email_scheduler_settings',array()),
    '#multiple' => TRUE,
    '#description' => t('This will attach addtional form elements to the profile page.'),
    '#required' => FALSE,
  );

  show_status_elements($form, $form_state);
  $form['#submit'][] = 'rebuild_menu_submit';
  return system_settings_form($form);

}





function rebuild_menu_submit(){
  variable_set('menu_rebuild_needed', TRUE);
}

function build_config_elements (&$form, &$form_state){

  $type ='';
  if(isset($form['#user']->name)){ //check if user
    $id = $form['#user']->uid;
    $type = 'User';
  } elseif(isset($form['name']['#default_value'])){ //check if roles
    $id = $form['rid']['#value'];
    $type = 'Roles';
  }


  $current_options = option_operations($type,$id);

  if(empty($current_options) === false){
    $current_option = $current_options['item'];
    $cron_enable = $current_option->enable;
    $email_subject = $current_option->subject;
    $email_body = $current_option->body;
    $run_date = $current_option->date;
    $run_status = $current_option->send_status;
  } else{
    $current_option = [];
    $cron_enable = false;
    $email_subject = '';
    $email_body = '';
    $run_date = '';
    $run_status = false;
  }
  $form['enable'] =[
    '#type' => 'checkbox',
    '#title' => t('Enable sending emails'),
    '#default_value' =>$cron_enable,
  ];
  $form['send_status'] =[
    '#type' => 'hidden',
    '#title' => t('Please enter your subject'),
    '#description' => t('Enter email subject'),
    '#default_value' => $run_status,
  ];
  $form['umid'] =[
    '#type' => 'hidden',
    '#title' => t('Please enter your subject'),
    '#description' => t('Enter email subject'),
    '#default_value' => $id,
  ];
  $form['subject'] =[
    '#type' => 'textfield',
    '#title' => t('Please enter your subject'),
    '#description' => t('Enter email subject'),
    '#default_value' => $email_subject,
    '#required' => true,
  ];
  $form['body'] =[
    '#type' => 'textarea',
    '#title' => t('Please enter email body content'),
    '#description' => t('Enter email text body'),
    '#default_value' => $email_body,
    '#required' => true,
  ];
  $form['frequency'] = array(
    '#type' => 'radios',
    '#options' => array(
      'day'   => t('Every day'),
      'week'  => t('Every week'),
      'month' => t('Every month'),
      'year'  => t('Every year'),
    ),
    '#default_value' => isset($run_date['frequency']) ? $run_date['frequency']:'',
    '#title' => t('Please select the frequency'),
    '#ajax' => [
      'callback' => 'update_message',
      'wrapper' => 'run-date-message',
    ],
    '#required' => true,
    
  );
  $form['group_date'] = array(
      '#type' => 'fieldset',
      '#title' => t('Run date'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,  
    );
  $form['group_date']['message'] =[
     '#markup' =>  t('Please select any of above options.'),
     '#prefix' => '<div id="run-date-message">',
     '#suffix' => "</div>",
  ];
  $month_names = array(
        1 => 'Jan', 
        2 => 'Feb',
        3 => 'Mar',
        4 => 'Apr', 
        5 => 'May', 
        6 => 'Jun',
        7 => 'Jul', 
        8 => 'Aug', 
        9 => 'Sep', 
        10 => 'Oct', 
        11 => 'Nov', 
        12 => 'Dec'
      );
  $form['group_date']['month'] =[
    '#type' => 'select',
    '#title' => t('Select the month'),
    '#default_value' =>  isset($run_date['month'])?$run_date['month']:'',
    '#options' => $month_names,
    '#states' => [
      'visible' => [
        ':input[name="frequency"]' => [
          ['value' => 'year'],
        ],
      ],

    ],
    '#ajax' => [
      'callback' => 'update_day_value',
      'wrapper' => 'run-day',
    ],
  ];


  $days = [];
  for ($i=1; $i <= 31; $i++) { 
    $days = $days + [$i => $i];
  }

  $form['group_date']['day'] =[
    '#type' => 'select',
    '#title' => t('Select the date'),
    '#prefix' => '<div id="run-day">',
    '#suffix' => "</div>",
    '#options' =>  $days,
    '#default_value' =>  isset($run_date['day']) ?$run_date['day']:'',
    '#states' => [
      'visible' => [
        ':input[name="frequency"]' => [
          ['value' => 'year'],
          ['value' => 'month'],
        ],
      ],
    ],
  ];
  $week_names = array(
        1 => 'Mon', 
        2 => 'Tue',
        3 => 'Wed',
        4 => 'Thur', 
        5 => 'Fri', 
        6 => 'Sat',
        7 => 'Sun', 
      );
  $form['group_date']['week'] =[
    '#type' => 'select',
    '#title' => t('Select the day'),
    // '#default_value' => $run_date,
    '#prefix' => '<div id="run-week">',
    '#suffix' => "</div>",
    '#options' =>   $week_names,
    '#default_value' =>  isset($run_date['week'])?$run_date['week']:'',
    '#states' => [
      'visible' => [
        ':input[name="frequency"]' => [
          'value' => 'week',
        ],
      ],

    ],
  ];
  
}

function update_message($form, $form_state) {

  if($form_state['values']['frequency'] === 'month'){
    $form['group_date']['message']['#markup'] = t('Please consider the number of days in month !!!');
    }
  else if ($form_state['values']['frequency'] === 'day'){
    $form['group_date']['message']['#markup'] = t('Every day');
  }
  else if ($form_state['values']['frequency'] === 'week'){
    $form['group_date']['message']['#markup'] = t('Every week');
  }
  else if ($form_state['values']['frequency'] === 'year'){
    $form['group_date']['message']['#markup'] = t('Every year');
  }
  else {
    $form['group_date']['message']['#markup'] = t('Please select the options below.');
  }
  return $form['group_date']['message'];
}
function update_day_value($form, $form_state) {
  $current_year = date('Y');
  if($form_state['values']['frequency'] === 'month'){
    $number_of_days = 31;

  } else{
    $number_of_days = cal_days_in_month(CAL_GREGORIAN, $form_state['values']['month'], $current_year);
  }

  $days = [];

  for ($i=1; $i <= $number_of_days; $i++) { 
    $days = $days + [$i => $i];
  }

  $form['group_date']['day']['#options'] = $days;
  return $form['group_date']['day'];
}



function show_status_elements(&$form, &$form_state){

  $show_options = [
    'All' => 'All',
    'Roles' => 'Roles',
    'Users' => 'Users'
  ];

  $form['email_scheduler_show'] =[
    '#type' => 'select',
    '#options' => $show_options,
    '#ajax' => [
      'callback' => 'show_status_callback',
      'wrapper' => 'status_table',
      'effect' => 'fade',
      'event' => 'change',
    ]
  ];

  $form['email_scheduler_remove'] =[
    '#type' => 'button',
    '#value' => t('Remove all records'),
    '#ajax' => [
      'callback' => 'remove_all',
      'wrapper' => 'status_table',
      'effect' => 'fade',
      'event' => 'click',
    ],

  ];
  $current_options = option_operations();
  $header_options = [
      t('Type'),
      t('Name'),
      t('Enable'),
      t('Frequency '),
      t('Date'),
      t('Sent today? '),
      t('Last run'),
      t('Edit')
    ];
  $form['email_scheduler_show_box'] =[
    '#theme' => 'table',
    '#rows' =>  status_output_format($current_options),
    '#prefix' => '<div id="status_table">',
    '#suffix' => '</div>',
    '#header' =>  $header_options,
  ];
}


function show_status_callback(&$form, &$form_state){

  return show_status($form, $form_state);
}

function create_bulk_buttons(&$form, &$form_state){
  $form['status_enable_button']  = [
     '#type' => 'select',
     '#options' => $options,
     '#id' => 'status_enable_button',
     '#name' => 'status_enable_button',
     '#default_value' =>  $eanble_button_value,
     '#prefix' => '<div id="status_enable_button">',
     '#suffix' => '</div>',
     '#ajax' => [
       'callback' => 'update_mail_status',
       'event' => 'change',

     ],
  ];
  return $form['status_enable_button'];

}

function show_status(&$form, &$form_state,$type = null){

  create_bulk_buttons($form, $form_state);


  $show_select = $form['email_scheduler_show']['#value'];



  $header_options = [
      t('Type'),
      t('Name'),
      t('Enable'),
      t('Frequency '),
      t('Date'),
      t('Sent today? '),
      t('Last run'),
      t('Edit')
    ];
  $show_box = $form['email_scheduler_show_box'];
    switch ($show_select) {
      case 'Users':
        $current_options = option_operations('User');
        break;
      case 'Roles':
        $current_options = option_operations('Roles');
        break;
      
      default:
        $current_options = option_operations();
        break;
    }
    
    if(count($current_options) > 0){
      $show_box['#header'] =  $header_options;
      $show_box['#rows'] =  status_output_format($current_options, $buttons);
    } else{
      $header_options = [
          t('Message'),
      ];
      $show_box['#header'] =  $header_options;
      $show_box['#rows']  = [[t("Sorry there's no results"),],];  
    }
 
  return $show_box;

}

 ?>