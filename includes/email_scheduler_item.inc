<?php 
// namespace mail_notification;

/**
* roles email object
*/
class noti_item
{
// system variable


// object variable
var $type;
var $item_name;
var $name_id;
var $enable;
var $subject;
var $body;
var $date;
var $last_run;
var $send_status;

  function __construct($type = "") {
    $this->send_status = FALSE;
 
  }
  public function __get($name) {
    return $this->$name;
  }

  public function __set($name,$value) {
    $this->name = $value;
  }
  public function show_info() {
    print '<p>type is '. $this->type . '</p>';
    print '<p>name is '. $this->name. '</p>';
    print '<p>name_id is '. $this->name_id. '</p>';
    print '<p>enable is '. $this->enable. '</p>';
    print '<p>mail_subject is '. $this->subject. '</p>';
    print '<p>mail_body is '. $this->body. '</p>';
    print '<p>mail_date is '. $this->date. '</p>';
    print '<p>send_status is '. $this->send_status . '</p>';
  }
}

 ?>