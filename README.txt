# Email scheduler version 1.0  #

This modules allows the configure of email notifications based on user roles. 

### How do I get this set up? ###

* Install and enable the module as normal
* Select the type that you need to config to send emails (/admin/config/email_scheduler)
* Go to user(/admin/people) or role (/admin/people/permissions/roles) to configure the settings

### Completed ###

1. Make each individual array as an object  
2. Using drupal date field on config page (Use verlidation of the field value rather than date field)
3. Add frequency(Every day / Every week / Every month / Every year)
4. Create a config page and have below two options avaiable (Roles / Individual Users)
5. Send email between 8AM to 6PM 

### Next version ###

* Token support
* Improve the instructions on the config form
* Log of emails sent?

### Who do I talk to? ###

* Shigang Xing 
* QQ: 2666122
* Email: matt@captovate.com.au
